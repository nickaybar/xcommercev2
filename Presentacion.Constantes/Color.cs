﻿namespace Presentacion.Constantes
{
    public static class Color
    {
        public static System.Drawing.Color ColorLetraMenu = System.Drawing.Color.Black;
        public static System.Drawing.Color ColorMenu = System.Drawing.Color.Ivory;

        //colores foco
        public static System.Drawing.Color ColorControlConFoco = System.Drawing.Color.Beige;
        public static System.Drawing.Color ColorControlSinFoco = System.Drawing.Color.White;
    }
}
