﻿using Presentacion.Core.Empleado;
using System.Windows.Forms;
using Presentacion.Core.Cliente;
using Presentacion.Core.Localidad;
using Presentacion.Core.Provincia;
using Presentacion.Core.ListaPrecio;
using Presentacion.Helpers;
using Presentacion.Seguridad.Usuario;
using Presentacion.Core.Rubro;
using Presentacion.Core.Marca;

namespace XCommerce
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void consultaDeEmpleadosToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fEmpleados = new _00001_Empleados();
            fEmpleados.Show();
        }

        private void consultaToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fProvincia = new _00005_Provincia();
            fProvincia.ShowDialog();
        }

        private void nuevaProvinciaToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fNuevaProvincia = new _00006_Provincia_ABM(TipoOperacion.Nuevo);
            fNuevaProvincia.ShowDialog();
        }

        private void consultaToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            var fLocalidad = new _00007_Localidad();
            fLocalidad.ShowDialog();
        }

        private void nuevaLocalidadToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fNuevaLocalidad = new _00008_Localidad_ABM(TipoOperacion.Nuevo);
            fNuevaLocalidad.ShowDialog();
        }

        private void nuevoEmpleadoToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fNuevoEmpleado = new _00002_ABM_Empleados(TipoOperacion.Nuevo);
            fNuevoEmpleado.ShowDialog();
        }

        private void consultaToolStripMenuItem2_Click(object sender, System.EventArgs e)
        {
            var fClientes = new _00003_Clientes();
            fClientes.ShowDialog();
        }

        private void nuevoClienteToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fClienteAbm = new _00004_ABM_Cliente(TipoOperacion.Nuevo);
            fClienteAbm.ShowDialog();
        }

        private void usuariosToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fUsuario = new _00009_Usuarios();
            fUsuario.ShowDialog();
        }

        private void administrarListasToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fListaPrecio = new _00010_ListaPrecio();
            fListaPrecio.ShowDialog();
        }

        private void consultaToolStripMenuItem4_Click(object sender, System.EventArgs e)
        {
            var fRubro = new _00011_Rubro();
            fRubro.ShowDialog();
        }

        private void nuevoRubroToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fRubroAbm = new _00012_Rubro_ABM(TipoOperacion.Nuevo);
            fRubroAbm.ShowDialog();
        }

        private void consultaToolStripMenuItem5_Click(object sender, System.EventArgs e)
        {
            var fMarca = new _00009_Marca();
            fMarca.ShowDialog();
        }

        private void nuevaMarcaToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var fMarcaAbm = new _00010_Marca_ABM(TipoOperacion.Nuevo);
            fMarcaAbm.ShowDialog();
        }
    }
}