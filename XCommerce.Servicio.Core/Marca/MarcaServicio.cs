﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCommerce.AccesoDatos;
using XCommerce.Servicio.Core.Marca.DTOs;

namespace XCommerce.Servicio.Core.Marca
{
    public class MarcaServicio : IMarcaServicio
    {
        public void Eliminar(long marcaId)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                var marcaEliminar = context.Marcas
                    .FirstOrDefault(x => x.Id == marcaId);

                if (marcaEliminar == null)
                    throw new Exception("Ocurrio un error al Obtener la Marca");

                marcaEliminar.EstaEliminado = true;

                context.SaveChanges();
            }
        }

        public long Insertar(MarcaDto Marcadto)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                var marcaNueva = new AccesoDatos.Marca
                {
                    Descripcion = Marcadto.Descripcion
                };

                context.Marcas.Add(marcaNueva);

                context.SaveChanges();

                return marcaNueva.Id;
            }
        }

        public void Modificar(MarcaDto Marcadto)
        {
            using (var context = new ModeloXCommerceContainer())
            {

                var marcaModificar = context.Marcas
                    .FirstOrDefault(x => x.Id == Marcadto.Id);

                if (marcaModificar == null)
                    throw new Exception("Ocurrio un error al Obtener la marca");

                marcaModificar.Descripcion = Marcadto.Descripcion;

                context.SaveChanges();


            }
        }

        //----------------------------------------------------------------------------------//

        public IEnumerable<MarcaDto> Obtener(string cadenaBuscar)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                return context.Marcas
                    .AsNoTracking()
                    .Where(x => x.Descripcion.Contains(cadenaBuscar))
                    .Select(x => new MarcaDto
                    {
                        Id = x.Id,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado
                    }).ToList();
            }
        }

        public MarcaDto ObtenerPorId(long entidadId)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                return context.Marcas
                    .AsNoTracking()
                    .Select(x => new MarcaDto
                    {
                        Id = x.Id,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado
                    }).FirstOrDefault(x => x.Id == entidadId);
            }
        }
    }
}
