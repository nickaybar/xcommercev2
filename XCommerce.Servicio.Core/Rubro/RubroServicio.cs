﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCommerce.AccesoDatos;
using XCommerce.Servicio.Core.Rubro.DTOs;

namespace XCommerce.Servicio.Core.Rubro
{
    public class RubroServicio : IRubroServicio
    {
        public void Eliminar(long rubroId)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                var rubroEliminar = context.Rubros.FirstOrDefault(x => x.Id == rubroId);

                if (rubroEliminar == null)
                    throw new Exception("Ocurrio un error al Obtener el rubro");

                rubroEliminar.EstaEliminado = true;

                context.SaveChanges();

            }
        }

        public void Insertar(RubroDto rubroDto)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                var rubro = new AccesoDatos.Rubro
                {
                    Descripcion = rubroDto.Descripcion
                };

                context.Rubros.Add(rubro);

                context.SaveChanges();


            }
        }

        public void Modificar(RubroDto rubroDto)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                var rubromodificar = context.Rubros.FirstOrDefault(x => x.Id == rubroDto.Id);

                if (rubromodificar == null)
                    throw new Exception("Ocurrio un error al Obtener el rubro");

                rubromodificar.Descripcion = rubroDto.Descripcion;

                context.SaveChanges();

            }
        }

        public IEnumerable<RubroDto> Obtener(string cadenaBuscar)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                return context.Rubros
                    .AsNoTracking()
                    .Where(x => x.Descripcion.Contains(cadenaBuscar))
                    .Select(x => new RubroDto
                    {
                        Id = x.Id,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado
                    }).ToList();
            }
        }

        public RubroDto ObtenerPorId(long entidadId)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                return context.Rubros
                    .AsNoTracking()
                    .Select(x => new RubroDto
                    {
                        Id = x.Id,
                        Descripcion = x.Descripcion,
                        EstaEliminado = x.EstaEliminado
                    }).FirstOrDefault(x => x.Id == entidadId);
            }
        }
    }
}
