﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Presentacion.FormularioBase;
using Presentacion.Helpers;
using XCommerce.Servicio.Core.ListaPrecioServicio;
using XCommerce.Servicio.Core.ListaPrecio.DTOs;
using XCommerce.Servicio.Core.ListaPrecio;

namespace Presentacion.Core.ListaPrecio
{
    public partial class _00011_ListaPrecio_ABM : FormularioAbm
    {
        private readonly IListaPrecioServicio _listaPrecioServicio;

        public _00011_ListaPrecio_ABM(TipoOperacion tipoOperacion, long? entidadId = null)
            : base(tipoOperacion, entidadId)
        {
            InitializeComponent();

            _listaPrecioServicio = new ListaPrecioServicio();

            if (tipoOperacion == TipoOperacion.Eliminar || tipoOperacion == TipoOperacion.Modificar)
            {
                CargarDatos(entidadId);
            }

            if (tipoOperacion == TipoOperacion.Eliminar)
            {
                DesactivarControles(this);
            }

            AsignarEventoEnterLeave(this);

            AgregarControlesObligatorios(txtDescripcion, "Descripción");
            AgregarControlesObligatorios(txtRentabilidad, "Rentabilidad");
            Inicializador(entidadId);
        }

        public override void Inicializador(long? entidadId)
        {
            if (entidadId.HasValue) return;

            // Asignando un Evento
            txtDescripcion.KeyPress += Validacion.NoSimbolos;
            txtDescripcion.KeyPress += Validacion.NoNumeros;

            txtRentabilidad.KeyPress += Validacion.NoSimbolos;
            txtRentabilidad.KeyPress += Validacion.NoLetras;
            txtDescripcion.Focus();
        }

        public override void CargarDatos(long? entidadId)
        {
            if (!entidadId.HasValue)
            {
                MessageBox.Show(@"Ocurrio un Error Grave", @"Error Grave", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                this.Close();
            }

            if (TipoOperacion == TipoOperacion.Eliminar)
            {
                btnLimpiar.Enabled = false;
            }

            var listaPrecio = _listaPrecioServicio.ObtenerPorId(entidadId.Value); // var provincia

            // Datos Personales
            txtDescripcion.Text = listaPrecio.Descripcion;
            //txtRentabilidad.Text =  listaPrecio.Rentabilidad;
        }

        public override bool EjecutarComandoNuevo()
        {
            if (!VerificarDatosObligatorios())
            {
                MessageBox.Show(@"Por favor ingrese los campos Obligatorios.", @"Atención", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }

            var nuevaLista = new ListaPrecioDto // nueva lista
            {
                Descripcion = txtDescripcion.Text,
                Rentabilidad = decimal.Parse(txtRentabilidad.Text)
            };

            _listaPrecioServicio.Insertar(nuevaLista);

            return true;
        }

        public override bool EjecutarComandoModificar()
        {
            if (!VerificarDatosObligatorios())
            {
                MessageBox.Show(@"Por favor ingrese los campos Obligatorios.", @"Atención", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }

            var listaParaModificar = new ListaPrecioDto //provincia modificar
            {
                Id = EntidadId.Value,
                Descripcion = txtDescripcion.Text,
                Rentabilidad = decimal.Parse(txtRentabilidad.Text)
                
              };

            _listaPrecioServicio.Modificar(listaParaModificar);

            return true;
        }

        public override bool EjecutarComandoEliminar()
        {
            if (EntidadId == null) return false;

            _listaPrecioServicio.Eliminar(EntidadId.Value);

            return true;
        }
    }
}