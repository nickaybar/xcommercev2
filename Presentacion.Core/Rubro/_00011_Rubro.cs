﻿using Presentacion.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XCommerce.Servicio.Core.Rubro;
using XCommerce.Servicio.Core.Rubro.DTOs;

namespace Presentacion.Core.Rubro
{
    public partial class _00011_Rubro : FormularioBase.FormularioConsulta
    {
        private readonly IRubroServicio _rubroServicio;

        public _00011_Rubro()
            : this(new RubroServicio())
        {
            InitializeComponent();
        }

        public _00011_Rubro(IRubroServicio rubroServicio)
        {
            _rubroServicio = rubroServicio;
        }

        public override void FormatearGrilla(DataGridView grilla)
        {
            base.FormatearGrilla(grilla);

            grilla.Columns["Descripcion"].Visible = true;
            grilla.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grilla.Columns["Descripcion"].HeaderText = @"Rubro";
            grilla.Columns["Descripcion"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            grilla.Columns["EstaEliminadoStr"].Visible = true;
            grilla.Columns["EstaEliminadoStr"].Width = 100;
            grilla.Columns["EstaEliminadoStr"].HeaderText = @"Eliminado";
            grilla.Columns["EstaEliminadoStr"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grilla.Columns["EstaEliminadoStr"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        public override void ActualizarDatos(DataGridView grilla, string cadenaBuscar)
        {
            grilla.DataSource = _rubroServicio.Obtener(cadenaBuscar);
        }

        //---------------------------------------------------------------------------------------------------------------//

        public override void EjecutarNuevo()
        {
            var fRubroAbm = new _00012_Rubro_ABM(TipoOperacion.Nuevo);
            fRubroAbm.ShowDialog();

            ActualizarSegunOperacion(fRubroAbm.RealizoAlgunaOperacion);

        }

        public override void EjecutarModificar()
        {
            base.EjecutarModificar();

            if (!PuedeEjecutarComando) return;

            if (!((RubroDto)EntidadSeleccionada).EstaEliminado)
            {
                var fRubroAbm = new _00012_Rubro_ABM(TipoOperacion.Modificar, EntidadId);

                fRubroAbm.ShowDialog();

                ActualizarSegunOperacion(fRubroAbm.RealizoAlgunaOperacion);
            }
            else
            {
                MessageBox.Show(@"El rubro se encuetra Eliminado", @"Atención", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        public override void EjecutarEliminar()
        {
            base.EjecutarEliminar();

            if (!PuedeEjecutarComando) return;

            if (!((RubroDto)EntidadSeleccionada).EstaEliminado)
            {
                var fRubroAbm = new _00012_Rubro_ABM(TipoOperacion.Eliminar, EntidadId);

                fRubroAbm.ShowDialog();

                ActualizarSegunOperacion(fRubroAbm.RealizoAlgunaOperacion);
            }
            else
            {
                MessageBox.Show(@"El rubro se encuetra Eliminado", @"Atención", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }
        //-------------------------------------------------------------------------------------------------//

        private void ActualizarSegunOperacion(bool realizoAlgunaOperacion)
        {
            if (realizoAlgunaOperacion)
            {
                ActualizarDatos(dgvGrilla, string.Empty);
            }
        }
    }
}