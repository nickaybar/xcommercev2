﻿using System;
using System.Collections.Generic;
using System.Linq;
using XCommerce.AccesoDatos;
using XCommerce.Servicio.Seguridad.Usuario.DTOs;
using System.Data.Entity;
using static Presentacion.Helpers.CadenaCaracteres;

namespace XCommerce.Servicio.Seguridad.Usuario
{
    public class UsuarioServicio : IUsuarioServicio
    {
        public void CambiarEstado(string nombreUsuario, bool estado)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                var usuario = context.Usuarios
                    .FirstOrDefault(usu => usu.Nombre == nombreUsuario);

                if(usuario == null)
                    throw new Exception($"No se encontro el Usuario: {nombreUsuario}.");

                usuario.EstaBloqueado = estado;

                context.SaveChanges();
            }
        }

        public void Crear(long personaId, string apellido, string nombre)
        {
            var cantidadLetras = 1;

            var nombreUsuario = CrearNombreUsuario(apellido, nombre, cantidadLetras);

            using (var context = new ModeloXCommerceContainer())
            {
                while (context.Usuarios.Any(x => x.Nombre == (string)nombreUsuario))
                {
                    cantidadLetras++;

                    nombreUsuario = CrearNombreUsuario(apellido, nombre, cantidadLetras);
                }

                var nuevoUsuario = new AccesoDatos.Usuario
                {
                    PersonaId = personaId,
                    Nombre = (string)nombreUsuario,
                    Password = Encriptar("P$assword"),
                    EstaBloqueado = false
                };

                context.Usuarios.Add(nuevoUsuario);

                context.SaveChanges();

            }
        }

        private object CrearNombreUsuario(string apellido, string nombre, int cantidadLetras)
        {
            var primeraParte = nombre.Trim().Substring(0, cantidadLetras).ToLower();

            var segundaParte = apellido.Trim().ToLower();

            var nombreUsuario = $"{primeraParte} {segundaParte}";

            return nombreUsuario;


        }

        public IEnumerable<UsuarioDto> Obtener(string cadenaBuscar)
        {
            using (var context = new ModeloXCommerceContainer())
            {
                return context.Personas
                    .AsNoTracking()
                    .Include(x => x.Usuarios)
                    .Where(x => x.Apellido.Contains(cadenaBuscar)
                    || x.Nombre.Contains(cadenaBuscar)
                    || x.Dni == cadenaBuscar)
                    .Select(x => new UsuarioDto
                    {
                        PersonaId = x.Id,
                        ApellidoPersona = x.Apellido,
                        NombrePersona = x.Nombre,
                        Nombre = x.Usuarios.Any()
                            ? x.Usuarios.FirstOrDefault().Nombre
                            : "NO ASIGNADO",
                        Id = x.Usuarios.Any()
                            ? x.Usuarios.FirstOrDefault().Id
                            : 0,
                        EstaBloqueado = x.Usuarios.Any()
                            ? x.Usuarios.FirstOrDefault().EstaBloqueado
                            : false,

                    })
                    .OrderBy(x => x.ApellidoPersona)
                    .OrderBy(x => x.NombrePersona)
                    .ToList();
                    
            }
        }
    }
}
