﻿using Presentacion.FormularioBase;
using System;
using System.Windows.Forms;
using XCommerce.Servicio.Seguridad.Usuario;
using XCommerce.Servicio.Seguridad.Usuario.DTOs;

namespace Presentacion.Seguridad.Usuario
{
    public partial class _00009_Usuarios : FormularioBase.FormularioBase
    { //
        private readonly IUsuarioServicio _usuarioServicio;
        private UsuarioDto _usuarioDto;

        public _00009_Usuarios()
        {
            InitializeComponent();
            _usuarioServicio = new UsuarioServicio();
            _usuarioDto = null;

            //Asigno imagen a los botones
            btnNuevo.Image = Constantes.Imagen.BotonNuevo;
            btnEliminar.Image = Constantes.Imagen.BotonEliminar;
            btnActualizar.Image = Constantes.Imagen.BotonActualizar;
        }

        private void _00009_Usuarios_Load(object sender, System.EventArgs e)
        {
            Actualizar(string.Empty);
            FormatearGrilla();
        }

        private void FormatearGrilla()
        {
            for (int i = 0; i < dgvGrilla.ColumnCount; i++)
            {
                dgvGrilla.Columns[i].Visible = false;
            }
            dgvGrilla.Columns["ApyNom"].Visible = true;
            dgvGrilla.Columns["ApyNom"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvGrilla.Columns["ApyNom"].HeaderText = @"Apellido y Nombre";
            dgvGrilla.Columns["ApyNom"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvGrilla.Columns["Nombre"].Visible = true;
            dgvGrilla.Columns["Nombre"].Width = 200;
            dgvGrilla.Columns["Nombre"].HeaderText = @"Nombre";
            dgvGrilla.Columns["Nombre"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvGrilla.Columns["EstaBloqueado"].Visible = true;
            dgvGrilla.Columns["EstaBloqueado"].Width = 100;
            dgvGrilla.Columns["EstaBloqueado"].HeaderText = @"Bloqueado";
            dgvGrilla.Columns["EstaBloqueado"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrilla.Columns["EstaBloqueado"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;


        }

        private void Actualizar(string cadenaBuscar)
        {
            dgvGrilla.DataSource = _usuarioServicio.Obtener(cadenaBuscar);
        }

        private void btnBuscar_Click(object sender, System.EventArgs e)
        {
            Actualizar(txtBuscar.Text);
        }

        private void btnActualizar_Click(object sender, System.EventArgs e)
        {
            Actualizar(string.Empty);
        }

        private void dgvGrilla_RowEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if(dgvGrilla.RowCount > 0)
            {
                _usuarioDto = (UsuarioDto)dgvGrilla.Rows[e.RowIndex].DataBoundItem;
            }
            else
            {
                _usuarioDto = null;
            }
        }

        private void btnNuevo_Click(object sender, System.EventArgs e)
        {
            if (_usuarioDto == null) MessageBox.Show("Por favor seleccione un registro");

            else
            {
                _usuarioServicio.Crear(_usuarioDto.PersonaId, _usuarioDto.ApellidoPersona, _usuarioDto.NombrePersona);

            }

            Actualizar(string.Empty);
        }

        private void btnEliminar_Click(object sender, System.EventArgs e)
        {
            if (_usuarioDto == null) MessageBox.Show("Por favor Seleccione un registro.");

            else
            {
                if (_usuarioDto.Nombre == "NO ASIGNADO") MessageBox.Show("La persona seleccionada no posee usuario");

                _usuarioServicio.CambiarEstado(_usuarioDto.Nombre, !_usuarioDto.EstaBloqueado);

                MessageBox.Show(_usuarioDto.EstaBloqueado
                    ? "El usuario fue Desbloqueado correctamente."
                    : "El usuario fue Bloqueado correctamente.");

            }

            Actualizar(string.Empty);
        }

        private void btnSalir_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void dgvGrilla_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < dgvGrilla.ColumnCount; i++)
            {
                dgvGrilla.Columns[i].Visible = false;
            }
            dgvGrilla.Columns["ApyNom"].Visible = true;
            dgvGrilla.Columns["ApyNom"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvGrilla.Columns["ApyNom"].HeaderText = @"Apellido y Nombre";
            dgvGrilla.Columns["ApyNom"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvGrilla.Columns["Nombre"].Visible = true;
            dgvGrilla.Columns["Nombre"].Width = 200;
            dgvGrilla.Columns["Nombre"].HeaderText = @"Nombre";
            dgvGrilla.Columns["Nombre"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvGrilla.Columns["EstaBloqueado"].Visible = true;
            dgvGrilla.Columns["EstaBloqueado"].Width = 100;
            dgvGrilla.Columns["EstaBloqueado"].HeaderText = @"Bloqueado";
            dgvGrilla.Columns["EstaBloqueado"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvGrilla.Columns["EstaBloqueado"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }
    }
}
